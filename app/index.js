'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');
var chalk = require('chalk');


var RocAppGenerator = yeoman.generators.Base.extend({
  init: function () {
    //
  },

  askFor: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay('Welcome to the marvelous RocApp generator!'));

    var prompts = [];

    prompts.push({
      name: 'roc_name',
      message: 'What is the name of your app ?'
    });

    this.prompt(prompts, function (props) {

      this.roc_name = props.roc_name;

      done();
    }.bind(this));
  },

  app: function () {
    this.mkdir(this.roc_name);
    this.mkdir(this.roc_name + '/javascripts');
    this.mkdir(this.roc_name + '/htmls');
    this.mkdir(this.roc_name + '/images');
    this.mkdir(this.roc_name + '/images/icons');
    this.mkdir(this.roc_name + '/images/tabs');
    this.mkdir(this.roc_name + '/javascripts/locales');
    this.mkdir(this.roc_name + '/javascripts/views');
    this.mkdir(this.roc_name + '/stylesheets');

    var self = this;
    var cp = function(path) {
      self.copy(path, self.roc_name + '/' + path);
    };
    var tpl = function(path) {
      self.template(path, self.roc_name + '/' + path);
    };

    tpl('app.roc.json');
    cp('README.md');

    // icons
    cp('images/icons/footer-home.png');
    cp('images/icons/footer-list.png');
    cp('images/icons/footer-more.png');
    cp('images/icons/footer-user.png');
    cp('images/icons/footer-home-active.png');
    cp('images/icons/footer-list-active.png');
    cp('images/icons/footer-more-active.png');
    cp('images/icons/footer-user-active.png');
    cp('images/tabs/active-background.png');
    cp('images/tabs/active-background@2x.png');

    cp('javascripts/app.handlebars.js');
    cp('javascripts/bootstrap.handlebars.js');
    cp('javascripts/locales/cn.js');
    cp('javascripts/views/account.handlebars.js');
    cp('javascripts/views/more.handlebars.js');
    cp('javascripts/views/home.handlebars.js');
    cp('javascripts/views/discover.handlebars.js');
    cp('javascripts/footer.handlebars.js');
    cp('javascripts/routes.handlebars.js');
    cp('htmls/index.handlebars.html');

    // css
    cp('stylesheets/animations.css');
    cp('stylesheets/bar.css');
    cp('stylesheets/base.css');
    cp('stylesheets/page_transitions.css');
    cp('stylesheets/tabs-darkgray.less.css');
  }
});

module.exports = RocAppGenerator;
