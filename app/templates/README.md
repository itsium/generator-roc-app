## Setup

### Roc installation

	git clone kgit@i8.no-ip.biz:fs.git roc/

### Development

	cd roc/
	grunt pack --watch true --cache false --config /path/to/app.roc.json

### Production

	cd roc/
	grunt pack --cache false --minify true --config /path/to/app.roc.json
