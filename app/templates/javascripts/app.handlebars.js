(function() {

    var _ns = {{ns}};

    window.onerror = function() {
        // @TODO send error to server
        alert(JSON.stringify(arguments));
    };

    window.clearKeyboard = function() {
        if (document.activeElement &&
            typeof document.activeElement.blur === 'function') {
            document.activeElement.blur();
        }
    };

    var app = new _ns.App({
        engine: 'ionic',
        onready: function() {

            var loadmask = App.Loadmask = new _ns.views.Loadmask({
                    lock: true
                });

            loadmask.compile();
            loadmask.render('body');
            loadmask.hide();

        }
    });

}());
