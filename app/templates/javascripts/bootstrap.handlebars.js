/* bootstrap */

{{classname "App.Settings"}} = new {{ns}}.Settings({
    serverUrl: {{#if minify}}'{{server}}'{{else}}window.server{{/if}},
    url: 'records/{id}.js'
});

{{classname "App.i18n"}} = new {{ns}}.helpers.i18n();
