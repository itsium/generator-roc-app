(function() {

    var _ns = {{ns}},
        _parent = _ns.views.Template;

    function _afterrender(self) {

        self.scroll = new _ns.physics.Scroll({
            autoResize: true,
            el: self.content.el,
            cmp: self,
            scrollable: 'y'
        });
        self.scroll.resize();

    }

    _parent.subclass({

        xtype: 'account',
        xtpl: 'container',
        listeners: {
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            opts.config = {
                cssClass: 'view-account'
            };

            opts.items = [{
                xtype: 'header',
                config: {
                    ui: 'red'
                },
                items: [{
                    xtpl: 'headertitle',
                    items: t('Account')
                }]
            }, {
                xtpl: 'content',
                ref: 'content',
                refScope: self,
                config: {
                    hasHeader: true,
                    padding: true
                },
                items: 'Account page'
            }];

            _parent.prototype.constructor.call(self, opts);
        }
    });

}());