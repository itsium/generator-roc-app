(function() {

    var _ns = {{ns}},
        _parent = _ns.views.Template;

    function _afterrender(self) {

        self.scroll = new _ns.physics.Scroll({
            autoResize: true,
            el: self.content.el,
            cmp: self,
            scrollable: 'y'
        });
        self.scroll.resize();

    }

    _parent.subclass({

        xtype: 'discover',
        xtpl: 'container',
        listeners: {
            afterrender: _afterrender
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            opts.config = {
                cssClass: 'view-discover'
            };

            opts.items = [{
                xtype: 'header',
                config: {
                    ui: 'red'
                },
                items: [{
                    xtpl: 'headertitle',
                    items: t('Discover')
                }]
            }, {
                xtpl: 'content',
                ref: 'content',
                refScope: self,
                config: {
                    hasHeader: true,
                    padding: true
                },
                items: 'Discover page'
            }];

            _parent.prototype.constructor.call(self, opts);
        }
    });

}());