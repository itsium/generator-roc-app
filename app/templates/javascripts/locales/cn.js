/* cn.js */

App.i18n.addLang('cn', {
  // Footer
  'Highlights': '推荐',
  'Discover': '分类',
  'Account': '账户',
  'More': '更多',
  'Back': ' '
});

App.i18n.setLang('cn');
