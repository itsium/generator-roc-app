{{classname "App.footer.Router"}} = new (function() {

    var _ns = {{ns}},
        _footer,
        _parent = _ns.RouterView,
        $ = _ns.Selector;

    function createFooter() {
        if (!_footer) {
            _footer = new _ns.views.Tabs({
                handler: false,
                implements: ['events'],
                listeners: {
                    afterrender: function(self) {
                        self.events.add('hideonscroll', {
                            xtype: 'events.direction',
                            el: self.el,
                            handler: function(direction) {
                                if (direction === 'up') {
                                    self.setVisible(true);
                                } else {
                                    self.setVisible(false);
                                }
                            }
                        });
                    }
                },
                config: {
                    cssClass: 'animated-tabs',
                    ui: 'darkgray',
                    iconPos: 'top'
                },
                items: [{
                    xtype: 'tabbutton',
                    config: {
                        text: t('Highlights'),
                        name: 'home',
                        icon: 'home',
                        route: '/home'
                    }
                }, {
                    xtype: 'tabbutton',
                    config: {
                        text: t('Discover'),
                        name: 'list',
                        icon: 'list',
                        route: '/discover'
                    }
                }, {
                    xtype: 'tabbutton',
                    config: {
                        text: t('Account'),
                        name: 'profile',
                        icon: 'user',
                        route: '/account'
                    }
                }, {
                    xtype: 'tabbutton',
                    config: {
                        text: t('More'),
                        name: 'more',
                        icon: 'more',
                        route: '/more'
                    }
                }]
            });
            _footer.visible = true;
            _footer.setVisible = function(visible) {

                var self = this;

                if (self.visible === visible) {
                    return;
                }

                self.visible = visible;

                if (visible) {
                    $.removeClass(self.el, 'slowhide');
                } else {
                    $.addClass(self.el, 'slowhide');
                }
            };
            _footer.compile();
            _footer.render('body');
            _footer.setActive('list');
            _footer.show();
        }
        return _footer;
    }

    _ns.global.on('ready', function() {
        createFooter();
    });

    return _parent.subclass({

        className: 'App.footer.Router',
        xtype: 'footer.router',

        routes: {
            '^/discover*': 'isDiscover',
            '^/home*': 'isHome',
            '^/account*': 'isUser',
            '^/more*': 'isMore'
        },
        routeEscape: false,
        routeNoMatch: 'isHome',

        mustHide: function() {
            _footer.events.get('hideonscroll').detach();
            _footer.setVisible(false);
        },

        isHome: function() {
            _footer.events.get('hideonscroll').attach();
            _footer.setVisible(true);
            _footer.setActive('home');
        },

        isDiscover: function() {
            _footer.events.get('hideonscroll').attach();
            _footer.setVisible(true);
            _footer.setActive('list');
        },

        isUser: function() {
            _footer.events.get('hideonscroll').detach();
            _footer.setActive('profile');
            _footer.setVisible(true);
        },

        isMore: function() {
            _footer.events.get('hideonscroll').detach();
            _footer.setVisible(true);
            _footer.setActive('more');
        }

    });

}())();