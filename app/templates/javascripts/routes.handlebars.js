(function() {

    var _ns = {{ns}},
        _parent = _ns.RouterView;

    var anim_config = {
        xtype: 'fx.animate.css',
        show: {
            speed: '.25s',
            me: 'fadeInDown',
            other: 'fadeOutDown'
        }
    };

    var router = _parent.subclass({

        routes: {
            '': 'routeMore',
            {{ route "/more" as="more" }}: 'routeMore',
            {{ route "/discover" as="discover" }}: 'routeDiscover',
            {{ route "/account" as="account" }}: 'routeAccount',
            {{ route "/home" as="home" }}: 'routeHome'
        },
        fx: {
            'more': anim_config,
            'home': anim_config,
            'discover': anim_config,
            'account': anim_config
        },

        routeMore: function() {
            this.setCurrentView('more');
        },

        routeDiscover: function() {
            this.setCurrentView('discover');
        },

        routeHome: function() {
            this.setCurrentView('home');
        },

        routeAccount: function() {
            this.setCurrentView('account');
        }

    });

    _ns.MoreRouter = new router();

}());